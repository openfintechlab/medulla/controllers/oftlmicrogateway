/**
  * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * @description
 * - Stalk test cases for testing framework components
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * 
 */


import axios        from "axios";
import express      from 'express';
import helmet       from "helmet"
import morgan       from "morgan";

import AppConfig    from "../../config/AppConfig";
import {AppHealth}  from "../../config/AppConfig";
import StaticVars   from "../../config/StaticVars";
import ExpressApp   from "../../utils/ExpressApp"
import router       from "../../routes/routes";




describe(`Application Bootstart Verification Test Cases`, () => {
    
    const _LISTENER_PORT:string = '3010';
    const _CONTEXT_ROOT:string = '/UTEST'
    const package_json = require(`../../../package.json`);

    beforeAll( ()=> {
        // Load configurations
        let loaded:boolean = AppConfig.validateConfig();
        expect(loaded).toBeTruthy();
    });

    test(`Wrong configuration, should not match`, ()=>{
        process.env['OFL_MED_PORT'] = _LISTENER_PORT;
        process.env[StaticVars.ConfigVars.contextRoot] = _CONTEXT_ROOT;
        expect(AppConfig.config.OFL_MED_PORT).not.toBe(3000);
        expect(AppConfig.config[StaticVars.ConfigVars.contextRoot]).not.toBe(package_json.name);
    })
   
    test(`Start express server on port ${_LISTENER_PORT} and context root ${_CONTEXT_ROOT} `, async () =>{        
        process.env['OFL_MED_PORT'] = _LISTENER_PORT;
        process.env[StaticVars.ConfigVars.contextRoot] = _CONTEXT_ROOT;
        expect(AppConfig.config.OFL_MED_PORT).toBe(_LISTENER_PORT);
        expect(AppConfig.contextRoot).toBe(_CONTEXT_ROOT);

        // Start the express server on _LISTENER_PORT and _CONTEXT_ROOT
        // and verify if it is getting         
        let  expressApp:any = new ExpressApp({
            contextRoot: AppConfig.contextRoot,    
            port: AppConfig.config.OFL_MED_PORT || StaticVars.DefaultValues.expressAppPort,
            middlewares: [
                helmet(),
                express.json(),
                express.urlencoded({ extended: true }),
                morgan('combined')
            ],
            router: router    
        });
        AppHealth.express_loaded = true;
        AppHealth.started = true;
        try{
            expressApp.startListener();
    
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_startup`
            });        
            // Status code of the readiness check should be 200
            expect(response.status).toBe(200);
            
            // Close the listener
            await expressApp.close();
        }catch(error){
            fail(error);            

        }finally{
            // Close the listener
            await expressApp.close();
        }        
    })

    test(`Readiness flag / App is not ready`, async () => {
        process.env['OFL_MED_PORT'] = _LISTENER_PORT;
        process.env[StaticVars.ConfigVars.contextRoot] = _CONTEXT_ROOT;
        expect(AppConfig.config.OFL_MED_PORT).toBe(_LISTENER_PORT);
        expect(AppConfig.contextRoot).toBe(_CONTEXT_ROOT);
        
        // Start the express server on _LISTENER_PORT and _CONTEXT_ROOT
        // and verify if it is getting         
        let  expressApp:any = new ExpressApp({
            contextRoot: AppConfig.contextRoot,    
            port: AppConfig.config.OFL_MED_PORT || StaticVars.DefaultValues.expressAppPort,
            middlewares: [
                helmet(),
                express.json(),
                express.urlencoded({ extended: true }),
                morgan('combined')
            ],
            router: router    
        });
        AppHealth.express_loaded = true;
        AppHealth.started = false;

        try{
            expressApp.startListener();
    
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_startup`
            });        
            // Status code of the readiness check should be 200
            
            
            fail(`Should not receive 200 response code`);  
        }catch(error){
            expect(error).toBeDefined();
            expect(error.response.status).toBeGreaterThanOrEqual(500);

        }finally{
            // Close the listener
            await expressApp.close();
        }        
    });

});


describe(`Readiness, Startup and Liveliness Tests`, () => {
    const _LISTENER_PORT:string = '3010';
    const _CONTEXT_ROOT:string = '/UTEST'
    const package_json = require(`../../../package.json`);
    var  expressApp:any;

    beforeAll(async () => {
        process.env['OFL_MED_PORT'] = _LISTENER_PORT;
        process.env[StaticVars.ConfigVars.contextRoot] = _CONTEXT_ROOT;

        // Start the express server on _LISTENER_PORT and _CONTEXT_ROOT
        // and verify if it is getting         
        expressApp = new ExpressApp({
            contextRoot: AppConfig.contextRoot,    
            port: AppConfig.config.OFL_MED_PORT || StaticVars.DefaultValues.expressAppPort,
            middlewares: [
                helmet(),
                express.json(),
                express.urlencoded({ extended: true }),
                morgan('combined')
            ],
            router: router    
        }); 

        try{
            expressApp.startListener();                
            AppHealth.express_loaded = true;
            AppHealth.started = true;
        }catch(error){            
            await expressApp.close(); // Close the connection and go home
            fail(error);            
        }    
    })

    afterAll(async () => {
        expressApp.close();
    })

    beforeEach(()=>{
        AppHealth.started = true;
        AppHealth.isLive = true;
        AppHealth.ready = true;
    })

    test(`Startup check Application is started`, async() => {

        try{                
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_startup`
            });        
            // Status code of the readiness check should be 200            
            expect(response.status).toBe(200);            
        }catch(error){
            fail(error);              

        }        
    });

    test(`Startup check Application is not started`, async() => {
        try{                
            AppHealth.started = false;
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_startup`
            });        
            // Status code of the readiness check should be 200            
            fail(`Should receive error from readiness startup check`)
        }catch(error){
            expect(error.response.status).toBeGreaterThanOrEqual(500);                        
        }        
    });

    test(`Startup check Application is ready`, async() => {

        try{                
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_readiness`
            });        
            // Status code of the readiness check should be 200            
            expect(response.status).toBe(200);            
        }catch(error){
            fail(error);              

        }        
    });

    test(`Startup check Application is not ready`, async() => {
        try{              
            AppHealth.ready = false;  
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_readiness`
            });        
            // Status code of the readiness check should be 200            
            fail(`Should not provide error code 200`);                                 
        }catch(error){            
            expect(error).toBeDefined();
            expect(error.response.status).toBeGreaterThanOrEqual(500);
        }        
    });

    test(`Liveliness check Application is alive`, async() => {
        try{                          
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_healthz`
            });        
            // Status code of the readiness check should be 200            
            expect(response.status).toBe(200);                                
        }catch(error){            
            fail(error);
        }        
    });

    test(`Liveliness check Application is not alive`, async() => {
        try{                          
            AppHealth.isLive = false;
            const response = await axios({
                "method": "get",
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "url": `http://localhost:${_LISTENER_PORT}/_healthz`
            });        
            // Status code of the readiness check should be 200                        
            fail(`Application is not suppoused to be alive`);
        }catch(error){            
            expect(error.response.status).toBeGreaterThanOrEqual(500);                                
        }        
    });

});