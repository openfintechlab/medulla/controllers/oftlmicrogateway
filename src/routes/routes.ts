/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                      from "express";
import logger                       from "../utils/Logger";
import OFTLRequestProcessor         from "../mapping/OFTLRequestProcessor";
import { PostRespBusinessObjects }  from "../mapping/bussObj/Response";
import OFTLLoginProcessor           from "../mapping/OFTLLoginProcessor";


const router: any = express.Router();


/**
 * Specific route for loging-ing
 */
router.post(`/login/:appID`, async(req:any, res: any) => {
    
    let oftlLoginProcessor:OFTLLoginProcessor = new OFTLLoginProcessor(req.body, req.params.appID);
    try{
        const response = await oftlLoginProcessor.login();
        res.status(response.status).send(response.data);     
    }catch(error){
        logger.error(`Error while authenticating a user`, error);
        if(error.code){
            res.status(error.code).send(error.data);
        }else{
            let resp = new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8902`, `Service not available`);
            res.status(502).send(resp);            
        }        
    }
});

/**
 * Generic route for HTTP-GET
 */
router.get('/:applicationID/:serviceVersion/:serviceName/*', async(req:any, res:any) => {        
    // URL will come in syntax: http://domain/{api/packagename}/{ApplicationID}/{serviceVersion}/{serviceName}/{serviceresources}
    // Step#1: Extract applicationID, serviceVersion and serviceName and get the URL without these components
    res.set("Content-Type","application/json; charset=utf-8");                         
    let oftlReqProcessor:OFTLRequestProcessor = OFTLRequestProcessor.getInstance();
    try{                        
        let filteredPath = await oftlReqProcessor.parse( req.baseUrl,
            req.params.applicationID,
            req.params.serviceVersion,
            req.params.serviceName,
            req.params[0],
            req.originalUrl,
            "GET",
            req.headers.authorization,
            req.query);
        
        logger.info(`Path filtered from the service call: ${filteredPath}`); // <-- fileteredPath will contain values with '/'                            
        let url: string = `${filteredPath}`;    
        // TODO! Load these options from configuration using HTTP-OPTIONS.{attribute}
        let options = {
            "timeout": 5000, // Fetch it from configuration
            "headers": {"Content-Type": "application/json"}
        };    
        const response = await oftlReqProcessor.doGet(url, options);        
        res.status(response.status).send(response.data);                                                            

    }catch(error){
        logger.error(`GET`, error,`Error while parsing the URL`);
        let resp = processError(error)
        res.status(resp.respCode).send(resp.respObj);                
    }
        
});



router.post('/:applicationID/:serviceVersion/:serviceName/*', async(req:any, res:any) => {        
     // URL will come in syntax: http://domain/{api/packagename}/{ApplicationID}/{serviceVersion}/{serviceName}/{serviceresources}
    // Step#1: Extract applicationID, serviceVersion and serviceName and get the URL without these components
    res.set("Content-Type","application/json; charset=utf-8");       
    let oftlReqProcessor:OFTLRequestProcessor = OFTLRequestProcessor.getInstance();                  
    try{
        let filteredPath = await oftlReqProcessor.parse( req.baseUrl,
            req.params.applicationID,
            req.params.serviceVersion,
            req.params.serviceName,
            req.params[0],
            req.originalUrl,
            "POST",
            req.headers.authorization,
            req.query);
        
        logger.info(`Path filtered from the service call: ${filteredPath}`); // <-- fileteredPath will contain values with '/'                            
        let url: string = `${filteredPath}`;    
        // TODO! Load these options from configuration using HTTP-OPTIONS.{attribute}
        let options = {
            "timeout": 5000, // Fetch it from configuration
            "headers": {"Content-Type": "application/json"}
        };    
        const response = await oftlReqProcessor.doPost(url, req.body, options);        
        res.status(response.status).send(response.data);                                         

    }catch(error){
        logger.error(`POST`,error,`Error while parsing the URL`);
        let resp = processError(error)
        res.status(resp.respCode).send(resp.respObj);                                                                 
    }
    
                                                                
});

router.delete('/:applicationID/:serviceVersion/:serviceName/*', async(req:any, res:any) => {        
    // URL will come in syntax: http://domain/{api/packagename}/{ApplicationID}/{serviceVersion}/{serviceName}/{serviceresources}
    // Step#1: Extract applicationID, serviceVersion and serviceName and get the URL without these components
    res.set("Content-Type","application/json; charset=utf-8");       
    let oftlReqProcessor:OFTLRequestProcessor = OFTLRequestProcessor.getInstance();                  
    try{
        let url: string = await oftlReqProcessor.parse( req.baseUrl,
            req.params.applicationID,
            req.params.serviceVersion,
            req.params.serviceName,
            req.params[0],
            req.originalUrl,
            "DELETE",
            req.headers.authorization,
            req.query);
        
        logger.info(`Path filtered from the service call: ${url}`); // <-- fileteredPath will contain values with '/'                                       
        // TODO! Load these options from configuration using HTTP-OPTIONS.{attribute}
        let options = {
            "timeout": 5000, // Fetch it from configuration
            "headers": {"Content-Type": "application/json"}
        };    
        const response = await oftlReqProcessor.doDelete(url, options);        
        res.status(response.status).send(response.data);           

    }catch(error){
        logger.error(`DELETE`,error,`Error while parsing the URL`);
        let resp = processError(error)
        res.status(resp.respCode).send(resp.respObj);                                                                   
    }  
                                                            
});

router.put('/:applicationID/:serviceVersion/:serviceName/*', async(req:any, res:any) => {        
    // URL will come in syntax: http://domain/{api/packagename}/{ApplicationID}/{serviceVersion}/{serviceName}/{serviceresources}
    // Step#1: Extract applicationID, serviceVersion and serviceName and get the URL without these components
    res.set("Content-Type","application/json; charset=utf-8");       
    let oftlReqProcessor:OFTLRequestProcessor = OFTLRequestProcessor.getInstance();                  
    try{
        let url: string = await oftlReqProcessor.parse( req.baseUrl,
            req.params.applicationID,
            req.params.serviceVersion,
            req.params.serviceName,
            req.params[0],
            req.originalUrl,
            'PUT',
            req.headers.authorization,
            req.query);
        
        logger.info(`Path filtered from the service call: ${url}`); // <-- fileteredPath will contain values with '/'                                       
        // TODO! Load these options from configuration using HTTP-OPTIONS.{attribute}
        let options = {
            "timeout": 5000, // Fetch it from configuration
            "headers": {"Content-Type": "application/json"}
        };    
        const response = await oftlReqProcessor.doPut(url, req.body, options);        
        res.status(response.status).send(response.data);                   

    }catch(error){
        logger.error(`PUT`,error,`Error while parsing the URL`);
        let resp = processError(error)
        res.status(resp.respCode).send(resp.respObj);                                                                 
    }      
                                                            
});


function processError(errorText:string): any{
    let resp = undefined;
    switch(errorText){
        case 'UNAUTHORIZED':
            resp = new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8901`, `You are not authorized to use this resource`);
            return {respCode: 401, respObj: resp}
        case 'TOKEN_NOT_FOUND':
            resp = new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8903`, `Required authorization info missing`);
            return {respCode: 400, respObj: resp} 
        case 'SESSION_NOT_FOUND':
            resp = new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8904`, `Invalid Session`);
            return {respCode: 401, respObj: resp} 
        case 'INVALID_TOKEN':
            resp = new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8904`, `Invalid Session`);
            return {respCode: 401, respObj: resp}        
        default:
            resp = new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8902`, `Service not available`);
            return {respCode: 502, respObj: resp}  
    }
}

export default router;