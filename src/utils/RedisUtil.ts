/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * @description
 * - Redis utility library
 */

import { AppHealth } from "../config/AppConfig";
import {Tedis}      from "tedis";
import logger       from "./Logger";


/**
 * @summary Utility class for managing all operation with Redis
 * @description
 * Singletone Utility call for managing all operation with Redis. Once 
 * instantiated, this class will connect with Redis using natice connection
 * and performs all operation.
 * **Note:**
 * - Process will exit in-case of any error while conencting or during connection
 * - Exit code 98 will be used in-case error is received from Redis server
 * - Exit code 99 will be returned in-case server closes the connection
 * @example
 * ```
 * let redisUtil:RedisUtil = RedisUtil.getInstance('redis_host', 'redis_port')
 * ```
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 */
export default class RedisUtil{
    private client!:Tedis;        
    private timeOut:number = 0;    
    private exitOnConnClose: boolean = true;    

    private static _instance: RedisUtil;

    private constructor(private hostName: string, 
                            private port:number, 
                            private password?: string){                                        
    }

    
    public static  getInstance(hostname?: string, port?:number, password?: string){        
        if(hostname === undefined || port === undefined){
            return this._instance;
        }else{
            return this._instance || (this._instance = new this(hostname,port,password));
        }        
    }
  


    public connect(): Promise<string>{
        return new Promise<string>((resolve:any, _:any) => {
            this.client = new Tedis({
                port: this.port,
                host: this.hostName,
                timeout: this.timeOut,
                password: this.password    
            });
            this.client.on("connect", () =>{
                logger.info(`EVENT`, `Connected to the Redis server`, this.hostName, this.port);
                logger.info("Connected to Redis server", 'host', this.hostName,'port', this.port);                
                resolve("Connected");
            });
            this.client.on("error", (message:any) => {
                logger.info(`EVENT`, `Error while / during redis connection`, message, this.hostName, this.port);
                logger.error("Error while connecting to redis server: ", message, this.hostName, this.port);
                AppHealth.isLive = false;                
                logger.error(`Fatal error. Exiting the application`,this.hostName, this.port);                
                process.exit(98);
            });
            this.client.on("close", (message:any) => {                
                logger.info(`EVENT`, `Close Connection with Redis Sever triggered`, message, this.hostName, this.port);
                if(this.exitOnConnClose){ // Safety switch as gracefulll disConnect() also call this event
                    logger.error(`Connection with the redis server closed`, message, this.hostName, this.port);
                    logger.error(`Quiting application`);    
                    AppHealth.isLive = false;                                
                    process.exit(99);
                }                
            });
        });
    }

    /**
     * Set a value in Redis basd on the service name     
     * @param value value:string representing the JSON object
     */
    public async set(serviceName: string, value:string){
        logger.info("Setting values with REDIS with size: "+ Buffer.byteLength(value, 'utf-8') + "(bytes)");
        await this.client.command("SET", serviceName, value);
    }

    /**
     * 
     * @param value value to set agains the service name. This is expected to be a JSON object
     * @param expiry Expiry in (number) seconds
     */
    public async setex(serviceName: string, value:string,expiry:number){
        logger.info("Setting values with REDIS with size: "+ Buffer.byteLength(value, 'utf-8') + "(bytes) and expiry: "+ expiry);
        await this.client.setex(serviceName,expiry,value);
    }

    /**
     * Gets value based on the service name
     * @param serviceName value:string Service name to fetch the configuration for
     */
    public async get(serviceName: string): Promise<string|number|null> {
        logger.info("Getting values for the key: "+ serviceName);
        return await this.client.get(serviceName);
    }

    /**
     * Deletes service configuration from redis
     */
    public async del(serviceName: string,): Promise<string|number|null> {
        logger.info("Deleting values for the key: "+ serviceName);
        return await this.client.del(serviceName);
    }

    public async ping(){
        console.log("IN PING")        
        let response = await this.client.command('PING');
        console.log("AFTER PING")        
        if(response === 'PONG'){
            return response;
        }else{
            throw Error('NOT CONNECTED');
        }
    }


    /**
     * Disconnects the connection
     */
    public disConnect(exitOnConnClose:boolean=false ){
        this.exitOnConnClose = exitOnConnClose;
        this.client.close();
    }

    /**
     * Get Redis client
     */
    get redisClient():Tedis{
        return this.client;
    }

}