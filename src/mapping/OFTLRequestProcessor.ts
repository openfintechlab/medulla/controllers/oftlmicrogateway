/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @description
 * Utility class for parsing the URL as per OFTL standard.
 * Reference: https://openfintechlab.atlassian.net/wiki/spaces/MM/pages/189071361/PCP+User+Authorization
 */

import axios                        from "axios";
import logger                       from "../utils/Logger";
import util                         from "util";
import { PostRespBusinessObjects }  from "./bussObj/Response";
import RedisUtil                    from "../utils/RedisUtil";
import jwt_decode                   from "jwt-decode";

type HTTP_METHODS = 'GET' | 'POST' | 'DELETE' | 'PUT' ;
 /**
  * Class for parsing URL component as per the required standard
  */
 export default class OFTLRequestProcessor{
     private readonly _ENDPOINT_CACH_KP: string = 'SESS';
     private static _instance:OFTLRequestProcessor;     
     

     private constructor(){}

     public static getInstance(){
         return this._instance || (this._instance = new this() );
     }

    /**
     * @summary Parse the request and return the filtered components in response
     * @description Parses the URL and provide the authorize endpoint by enquiring REDIS cacheserver
     * Throws following exception:
     * - **UNAUTHORIZED**: When specific application is not authroized to to access the specific resource
     * @param applicationID Application ID
     * @param serviceVersion Service Version
     * @param serviceName Service Name
     * @param params Request Params (e.g. req.params[0])
     * @param orignalURL Orignal URL, extracted by parsing req.orignalUrl
     * @param httpMethod HTTP Method (e.g. PUT,POST,GET,DELETE)
     * @param queryParams Query Parameters
     */
    public async parse(baseUrl:string, applicationID:string, 
                        serviceVersion:string,serviceName:string, 
                            params: string, orignalURL: string, 
                            httpMethod: HTTP_METHODS, 
                            jwtToken: string,
                            queryParams?:any){
        return new Promise<string>( async (resolve, reject) => {
            logger.debug(`Processing request for: ApplicationID:${applicationID} ServiceVersion:${serviceVersion} ServiceName: ${serviceName} params: ${params} orignalURL: ${orignalURL}`);
            if(baseUrl.length <= 0 || 
                applicationID.length <= 0 || 
                serviceVersion.length <= 0 || 
                serviceName.length <= 0 ||                 
                orignalURL.length <= 0){
                    reject('Invalid request');
                } else{                                        
                    try{
                        // Step#1: Validate if the JWT token is present in the header or not
                        if(jwtToken === undefined || jwtToken === null || jwtToken.length <= 0){
                            reject('TOKEN_NOT_FOUND');
                        }else{
                            jwtToken = jwtToken.replace(`Bearer `, '');
                        }
                        // Step#2: Validate the JWT token by taking response from `usermanagement` service component
                        // Step#3: Extract `session_state` from JWT_Token

                        // [FB20210201] Bug Fux! extractSession_State() will cause exception in-case invalid token 
                        // is provided
                        let session_state:string = this.extractSession_State(jwtToken);
                        // END;
                        let redisUtil = RedisUtil.getInstance()
                        let cacheKey:string = this._ENDPOINT_CACH_KP + '.' + applicationID + '.' + session_state;
                        let cacheResp = await redisUtil.get(cacheKey);
                        if( cacheResp === undefined  || cacheResp === null){
                            logger.error(cacheKey,`Not found in the cache server`)
                            reject(`SESSION_NOT_FOUND`);
                        }else{                            
                            let cacheRespJson = JSON.parse((<string>cacheResp));
                            // Step#4: Check if specific method is allowed or not
                            if(!this.checkACL(httpMethod,cacheRespJson,params, serviceName, serviceVersion)){
                                logger.error(`User not authorized to use the specific verb`);
                                reject(`UNAUTHORIZED`);
                            }                        
                            // Step#5: Pull out ctx_root_uri and hostname from the response
                            let aclKey:any = cacheRespJson[serviceName][serviceVersion];
                            if(aclKey.hostname.length <= 0 || aclKey.ctx_root_uri.length <=0 || aclKey.port.length <= 0){
                                logger.error(`Invalid ACL Object Retrived`,`Hostname or uri does not exist in the SESS `, cacheRespJson.hostName, cacheRespJson.ctx_root_uri,  cacheRespJson.port);
                                reject(`Required fields does not exist in EPCFG`);
                            }       
                            // Step#6: Construct a URL by loading elements received from the response                     
                            if(params.length > 1 && params.indexOf(`/`) === 0){
                                // removes the first occurrence of '/' from the params
                                params = params.substring(1,params.length);                                    
                            }
                            let url = new URL(aclKey.ctx_root_uri + '/' + (params || ''), aclKey.hostname);
                            url.host += ":" + aclKey.port;
                            if(queryParams != undefined || queryParams != null){
                                for(var attr in queryParams){
                                    url.searchParams.set(attr, queryParams[attr]);
                                }
                            }                            
                            resolve(url.toString());                                                        
                        }                                      
                    }catch(error){
                        logger.error(`Error while constructing URL`,`${util.inspect(error,{compact:true,colors:true, depth: null})}`)
                        // [FB20210102]: Create a new exception with error.message in-case it exist
                        if(error.message){
                            reject(error.message);
                        }else{
                            reject(error);
                        }                        
                    }                      
                }            
        });        
    }

     private checkACL(httpMethod: HTTP_METHODS, acl: any, inputParams: string, serviceName:string, serviceVersion:string): boolean {         
         if (acl[serviceName][serviceVersion][httpMethod] === undefined 
                || acl[serviceName][serviceVersion][httpMethod] === null 
                 || acl[serviceName][serviceVersion][httpMethod].length <= 0) {
             logger.error(`User not authorized to use the specific verb`);
             return false;
         } else {             
             // let exist:boolean = false;            
             for (let allowedParams of acl[serviceName][serviceVersion][httpMethod]) {
                 let allowParmNumb:number = allowedParams.replace(/\//g,'').split(':').filter(Boolean).length;
                 let inpParmNumb:number = inputParams.split(`/`).filter(Boolean).length;
                 // let exist = allowedParams.replace(/\//g,'').split(':').length === inputParams.split(`/`).length;
                 let exist = (allowParmNumb === inpParmNumb);
                 if (exist) {
                     logger.info(`Match found. Allowing app to proceed`)
                     return exist;
                 }
             }
             logger.error(`Input param is not in allowed list:`, httpMethod, acl[httpMethod]);
             return false;
         }
     }

     private extractSession_State(jwtToken:string):string{
        try{
            let decodedToken:any = jwt_decode(jwtToken);
            return decodedToken.session_state.replace(/-/g,'');
        }
        catch(error){
            throw new Error('INVALID_TOKEN');
        }
     }
    
    public async doGet(url: string, options:any){
        // TODO! Call HTTP Get, pass the URL and return response     
        // FIXME! Timeout value is not working in-case host is invalid   
        logger.debug(`Doing HTTP-GET on: ${url}`);
        options.baseURL = url;
        let axInst!: any;
        let response!:any;
        
        try{
            axInst = axios.create(options);    
            axInst.defaults.timeout = options.timeout;
            response = await axInst.get(url);
        }catch(error){            
            const hasResponse = (error.response)? true: false;
            let errObj: any = error.message;
            errObj = (!error.message)? ((error.response)? error.response.data : error.request.data ) : error.message;
            logger.debug(`Error while calling HTTP-GET function: ${util.inspect(errObj,{compact:true,colors:true, depth: null})}`)            
            if(hasResponse){
                response = error.response;
            }else {
                return this.getErrorRespObj(errObj);
            }
        }                
        return {status: response.status, data: response.data};
    }

    public async doDelete(url: string, options:any){
        // TODO! Call HTTP Get, pass the URL and return response     
        // FIXME! Timeout value is not working in-case host is invalid   
        logger.debug(`Doing HTTP-DELETE on: ${url}`);
        options.baseURL = url;
        let axInst!: any;
        let response!:any;
        
        try{
            axInst = axios.create(options);    
            axInst.defaults.timeout = options.timeout;
            response = await axInst.delete(url);
        }catch(error){            
            const hasResponse = (error.response)? true: false;
            let errObj: any = error.message;
            errObj = (!error.message)? ((error.response)? error.response.data : error.request.data ) : error.message;
            logger.debug(`Error while calling HTTP-DELETE function: ${util.inspect(errObj,{compact:true,colors:true, depth: null})}`)            
            if(hasResponse){
                response = error.response;
            }else {
                return this.getErrorRespObj(errObj);
            }
        }                
        return {status: response.status, data: response.data};
    }
    
    
    public async doPost(url: string, body: any, options:any){
        // TODO! Call HTTP Get, pass the URL and return response     
        // FIXME! Timeout value is not working in-case host is invalid   
        logger.debug(`Doing HTTP-POST on: ${url}`);
        options.baseURL = url;        
        let axInst!: any;
        let response!:any;
        
        try{
            axInst = axios.create(options);                
            response = await axInst.post(url, body);            
        }catch(error){            
            const hasResponse = (error.response)? true: false;
            let errObj: any = error.message;
            errObj = (!error.message)? ((error.response)? error.response.data : error.request.data ) : error.message;
            logger.debug(`Error while calling HTTP-POST function: ${util.inspect(errObj,{compact:true,colors:true, depth: null})}`)            
            if(hasResponse){
                response = error.response;
            }else {
                return this.getErrorRespObj(errObj);
            }
        }                
        return {status: response.status, data: response.data};
    }
    
    public async doPut(url: string, body: any, options:any){
        // TODO! Call HTTP Get, pass the URL and return response     
        // FIXME! Timeout value is not working in-case host is invalid   
        logger.debug(`Doing HTTP-PUT on: ${url}`);
        options.baseURL = url;        
        let axInst!: any;
        let response!:any;
        
        try{
            axInst = axios.create(options);                
            response = await axInst.put(url, body);            
        }catch(error){            
            const hasResponse = (error.response)? true: false;
            let errObj: any = error.message;
            errObj = (!error.message)? ((error.response)? error.response.data : error.request.data ) : error.message;
            logger.debug(`Error while calling HTTP-GET function: ${util.inspect(errObj,{compact:true,colors:true, depth: null})}`)            
            if(hasResponse){
                response = error.response;
            }else {
                return this.getErrorRespObj(errObj);
            }
        }                
        return {status: response.status, data: response.data};
    }

    public getErrorRespObj(error:any): any{
        const respObject: PostRespBusinessObjects.PostingResponse = new PostRespBusinessObjects.PostingResponse();    
        switch(error.code){
            case 'EAI_AGAIN':
                return {status: 502, data: respObject.generateBasicResponse("9751",`Specific address not found`)};
            case 'ENOTFOUND':
                return {status: 502, data: respObject.generateBasicResponse("9751",`Specific address not found`)};
            case 'ECONNABORTED':
                return {status: 502, data: respObject.generateBasicResponse("9752",`Response timed out`)};
            default:
                return {status: 502, data: respObject.generateBasicResponse("9750",`Unable to process the request`)};
        }
    }

 }