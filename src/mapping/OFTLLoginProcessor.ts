/**
 * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * @packageDocumentation
 * Utility class for creating ACL of the logged in user
 * Reference: https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/medulla/Physical-Architecture#url-scehema
 * **CHAGNES**
 * - [FB20210201]:
 *      1. Changed configuration key and made it environment variable family by removing "."  in following functions
 *          - getUserToken(): 
 *          - validateApplicationUserAssociation(): 
 *  
 */


import logger                       from "../utils/Logger";
import OFTLRequestProcessor         from "../mapping/OFTLRequestProcessor";
import AppConfig                    from "../config/AppConfig";
import RedisUtil                    from "../utils/RedisUtil";
import OracleDBInteractionUtil      from '../utils/OracleDBInteractionUtil';
import util                         from "util";
import { PostRespBusinessObjects }  from "../mapping/bussObj/Response";

export default class OFTLLoginProcessor {

    sqlStatements = {
        "SQL_SEL_001": `SELECT app.name, app.application_id, app.status, apusr.user_id,apusr.digest,apusr.status_code as user_status
                            from med_service_application app
                            INNER JOIN app_user apusr on apusr.application_id = app.application_id
                            where app.status = :appstatus AND apusr.status_code = :status_code AND app.name = :app_name AND user_id = :user_id`,
        "SQL_SQL_002": `select app.name,app.application_id, app.status, app.status_description AppStatus, asep.service_id, asep.endpoint_id,asep.version_id, 
                            srv.name service_name, srv.ctx_root_uri, srv.sdk_doc_uri,srv.api_doc_uri, srv.visibility,srv.type,srv.hostname, srv.port, msep.http_method, msep.endpoint_uri
                            from med_service_application app
                                INNER JOIN application_service_ep asep on 
                                    app.application_id = asep.application_id
                                INNER JOIN med_service srv on
                                    srv.srv_id = asep.service_id
                                INNER JOIN med_service_endpoints msep on
                                    asep.endpoint_id = msep.endpoint_id
                            where app.name = :app_name`
    }

    /**
     * @summary Instantiated Logs in
     * @param {any} requestJSON JSON request (request.body)
     * @param {string} appID Requestor application ID
     */
    constructor(private requestJSON: any,
                    private appID: string) {

    }

    /**
     * Authenticate and logs the user in the session
     */
    public login() {
        return new Promise<any>(async (resolve, reject) => {
            try {
                logger.info(`Login process initated for user `);
                // Step#1: Validate the request against in the database
                let validResult:boolean = await this.validateApplicationUserAssociation(this.appID,this.requestJSON.userDetails.username);
                if(! validResult){                    
                    reject({code: 404, data: new PostRespBusinessObjects.PostingResponse().generateBasicResponse(`8904`, `Invalid user`) });
                    return;
                }else{
                    logger.info(`User is associated with the specific application`);
                }                
                // Step#2: Call login method endpoint of the usermanagement service component
                const userLoginResp = await this.getUserToken(this.requestJSON);
                // Step#3: Store the session in the redis cache server
                if(userLoginResp.status === 200){
                    // Create ACL                                        
                    let aclObj = await this.createACLObject(this.appID, userLoginResp.data);                    
                    let key:string = `SESS.` + this.appID + '.' + userLoginResp.data.tokenDetails.session_state.replace(/-/g,'');
                    let redisUtil = RedisUtil.getInstance()
                    await redisUtil.setex(key, JSON.stringify(aclObj), <number>userLoginResp.data.tokenDetails.expiresInSecs);
                    resolve(userLoginResp);    
                }else{
                    reject({code: userLoginResp.status, data: userLoginResp.data });
                }                
            } catch (error) {
                reject(error);
            }
        });
    }

    private async getUserToken(requestJSON: any) {
        try {
            let oftlReqProcessor = OFTLRequestProcessor.getInstance();
            let options = {
                // [FB20210201]: Changed config "oftl.login.timeout" to "oftl_login_timeout" so that it becomes compatible
                // with environment variable formats
                "timeout": AppConfig.config['oftl_login_timeout'] * 1000, // Fetch it from configuration
                // End;
                "headers": { "Content-Type": "application/json" }
            };
            // [FB20210201]: Changed config "oftl.login.url" to "oftl_login_url" so that it becomes compatible
            // with environment variable formats
            const response = await oftlReqProcessor.doPost(AppConfig.config['oftl_login_url'], requestJSON, options);
            // END;
            return response;
        } catch (error) {
            throw error;
        }
    }

    private async validateApplicationUserAssociation(appID:string, userID:string){
        try{
            logger.info(`Validating existence of `, appID, userID);
            // [FB20210201]: Changed config variables and removed "." to make it environment variable friendly
            let binds = [AppConfig.config['auth_value_valid_app_status'],
                            AppConfig.config['auth_value_valid_user_status'],
                            appID,
                            userID ]
            let result = await OracleDBInteractionUtil.executeRead(this.sqlStatements.SQL_SEL_001,binds);
            return result.rows.length > 0;
        }catch(error){
            logger.error(`Error while validating the application and user association`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            throw error;
        }
    }

    private async createACLObject(appID:string, userLoginResp:any){
        let result;
        try{
            logger.info(`Creating business object `, appID);
            let binds = [appID];
            result = await OracleDBInteractionUtil.executeRead(this.sqlStatements.SQL_SQL_002,binds);
            if(result.rows.length < 0){                
                throw new Error('User does not have any app mapped');
            }else{
                let aclObj = this.convertDBToACL(result.rows, userLoginResp);
                return aclObj;
            }
        }catch(error){
            logger.error(`Error while creating acl`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`, `${result.rows.length | 0}`);
            throw error;
        }
    }

    private convertDBToACL(dbRows:any, userLoginResp:any){
        if(dbRows.length <= 0){
            throw new Error('convertDBToACL()/Invalid input/dbRows should be > 0');
        }
        var busObj:any = {// Common header fields
            "timeoutInSecs": 0,
            "uuid": "",
            "appID": "",
            "appName": ""                        
        }
        busObj.timeoutInSecs = userLoginResp.tokenDetails.expiresInSecs;
        busObj.uuid = userLoginResp.tokenDetails.userID;
        busObj.appID = dbRows[0].APPLICATION_ID;
        busObj.appName = dbRows[0].NAME;
        // TLDR; Iterate through each row and populate the buss object        
        for(let dbRow of dbRows){
            // Define the object if does not already exist;           
            if (busObj[dbRow.SERVICE_NAME] === undefined || busObj[dbRow.SERVICE_NAME][dbRow.VERSION_ID] === undefined) {
                busObj[dbRow.SERVICE_NAME] = {
                    [dbRow.VERSION_ID]: {
                        hostname: dbRow.HOSTNAME,
                        port: parseInt(dbRow.PORT),
                        ctx_root_uri: dbRow.CTX_ROOT_URI,
                        api_doc_uri: dbRow.API_DOC_URI,
                        visibility: dbRow.VISIBILITY,
                        type: dbRow.TYPE,
                        GET: [],
                        PUT: [],
                        DELETE: [],
                        PATCH: [],
                        POST: []
                    }
                }
                
            }                  
            busObj[dbRow.SERVICE_NAME][dbRow.VERSION_ID][dbRow.HTTP_METHOD.toUpperCase()].push(dbRow.ENDPOINT_URI);            
        }        
        // END;
         return busObj;
    }
    
}