/**
  * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * @description
 * - Root entry level file for bootstarting node js application
 * - Load configuration JSON from local environment
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * 
 */
import express      from 'express';
import chalk        from "chalk";
import helmet       from "helmet"
import morgan       from "morgan";

import ExpressApp               from "./utils/ExpressApp"
import router                   from "./routes/routes";
import logger                   from "./utils/Logger";
import AppConfig                from "./config/AppConfig";
import {AppHealth}              from "./config/AppConfig";
import util                     from "util";
import StaticVars               from './config/StaticVars';
import OracleDBInteractionUtil  from './utils/OracleDBInteractionUtil';
import RedisUtil                from "./utils/RedisUtil";


/**
 * @summary Main entry point class responssible for bootstarting sub-components
 * @class Main
 */
export abstract class Main{
    private static package_json = require('../package.json');
    private static app:ExpressApp;
    private static server:any;
    
    /**
     * @summary Main function for boot starting the application
     * @description Process of bootstart includes:
     * 1. Displaying banner
     * 2. Fetching configuration from the config store
     * 3. Starting Express
     * 4. Setting startup check
     * @function @name start()     
     */    
    public static async start(){
        try{
            // Step#1: Display the banner
            this.displayBanner();
            // Step#2: Validate loading of service configurations
            AppConfig.validateConfig();
            // Step#3: Connect with the database server
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
            // Connect with Redis
            await RedisUtil.getInstance(AppConfig.config.OFL_MED_REDIS_HOST, AppConfig.config.OFL_MED_REDIS_PORT).connect();
            // Step#4: start express app                  
            this.startAppListener();                           
            // Step#5: Mark app as ready to take the traffic
            AppHealth.started = true;
            AppHealth.ready = true;
            AppHealth.isLive = true;
            AppHealth.express_loaded = true;            
            logger.info(`Application started successfully....`)
        }catch(error){
            logger.debug(`Error while bootstarting app`,`${util.inspect(error,{compact:true,colors:true, depth: null})}`);            
            AppHealth.isLive = false; // Reload            
        }
    }



    /**
     * @summary Displays banner information of the sevice component using logger
     * @function
     */
    public static displayBanner(){        
        console.log(chalk.bold.yellow(`***************************************************`));
        console.log(chalk.bold.yellow(`┌─┐┌─┐┌─┐┌┐┌┌─┐┬┌┐┌┌┬┐┌─┐┌─┐┬ ┬┬  ┌─┐┌┐  ┌─┐┌─┐┌┬┐`));
        console.log(chalk.bold.yellow(`│ │├─┘├┤ │││├┤ ││││ │ ├┤ │  ├─┤│  ├─┤├┴┐ │  │ ││││`));
        console.log(chalk.bold.yellow(`└─┘┴  └─┘┘└┘└  ┴┘└┘ ┴ └─┘└─┘┴ ┴┴─┘┴ ┴└─┘o└─┘└─┘┴ ┴`));
        console.log(chalk.bold.yellow(`***************************************************`));
        console.log(chalk.yellow(`Copyright 2020-2022 Openfintechlab, Inc. All rights reserved`));

        console.log(chalk.blue(`Service Name: ${chalk.magenta(Main.package_json.name)}`));
        console.log(chalk.blue(`Service Version: ${chalk.magenta(Main.package_json.version)}`));
        console.log(chalk.blue(`Service Description: ${chalk.magenta(Main.package_json.description)}`));
        console.log(chalk.blue(`Source: ${chalk.magenta(Main.package_json.repository.url)}`));                        
    }

    /**
     * @summary Starts the express application listener
     * @function     
     */
    public static async startAppListener(){
        Main.app = new ExpressApp({
            contextRoot: AppConfig.contextRoot,    
            port: AppConfig.config.OFL_MED_PORT || StaticVars.DefaultValues.expressAppPort,
            middlewares: [
                helmet(),
                express.json(),
                express.urlencoded({ extended: true }),
                morgan('combined')
            ],
            router: router    
        });
        this.server = this.app.startListener();                
    }

    public static get expressApp(){
        return this.app;
    }
    
}


process.once('exit', (_) => async function() {    
    logger.info("[EXIT] Existing the app");
    try{
        AppHealth.isLive = false;
        await OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.close();
        process.exit(1);   
    }
});

process.once('SIGINT', async function() {
    logger.info("[SIGINT] Disconnecting all listeners");
    try{
        AppHealth.isLive = false;
        OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.close();
        process.exit(1);   
    }
});

process.once('SIGTERM', async function() {
    logger.info("[SIGTERM] Disconnecting all listeners");
    try{
        AppHealth.isLive = false;
        OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.close();
        process.exit(1);   
    }
        
});

process.once('SIGHUP', async function() {
    logger.info("[SIGHUP] Disconnecting all listeners");
    try{
        AppHealth.isLive = false;
        OracleDBInteractionUtil.closePoolAndExit();        
    }catch(error){
        logger.debug(`Error in SIGTERM:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
    }finally{
        await Main.expressApp.close();
        process.exit(1);   
    }
});


/**
 * Bootstarting express application
 */
const load = async() => {
    try{
        await Main.start();        
    }catch(error){
        logger.error(`Error received while loading the configuration:`, `${error}`);        
        AppHealth.isLive = false;
    }    
}; load(); /* Kick Start */
